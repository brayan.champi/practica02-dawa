import express from 'express'
import MongoDB from './lib/mongo'
import helmet from 'helmet'
import morgan from 'morgan'

import { config } from './config'

const mongo = new MongoDB()

const app = express()

app.use(morgan('common'))
app.use(helmet())
app.use(express.json())

app.get('/', (req, res) => {
    res.send('<h1>Hello World!</h1>')
})

app.post('/api', async (req, res) => {
    const { name } = req.body
    const entity = {
        name: name
    }
    await mongo.create('entity', entity)
    res.status(201).json(entity)
})

app.listen(config.port, () => {
    console.log(`Server running on port ${config.port}`)
})
